.. _accessories-peripherals:

Peripherals
############

.. note::
    Most Keyboards, Mouse, and USB Hubs are plug-n-play devices and they are 
    supported out of the box in linux. List below only shows what has been tested. 
    You may have something different and it will work without any additional software requirement.

Keyboard & Mouse Combo
***********************

With limited ports availability on BeagleBones it is recommended to use wireless 
Keyboard & Mouse combos.

- `Adafruit keyboard & Mouse w/batteries <https://www.mouser.in/ProductDetail/Adafruit/1738?qs=GURawfaeGuBoaqdx8E%2Fl7w%3D%3D>`_
- `Portronics Key2-A Combo of Multimedia Wireless Keyboard & Mouse <https://www.amazon.in/Portronics-Combo-Multimedia-Wireless-Light-Weight/dp/B07X1KRPDZ/>`_

Keyboards
************

**Make sure that you plug the keyboard into the USB Host connector before powering on the board.**

- `BTC USB 6100C <http://www.amazon.com/BTC-6100C-Compact-MultiMedia-Keyboard/dp/B000VITZ98/>`_
- `Inland USB 70010 <http://inlandproduct.com/usbwiredkeyboard.aspx>`_
- `Gear Head Wireless KB3800TPW <http://www.amazon.com/Wireless-Touch-Touchpad-Keyboard-Smart/dp/B003GU1028/>`_
- `Microsoft Wireless 800 <http://www.amazon.com/Microsoft-Wireless-Keyboard-800-2VJ-00001/dp/B004JO16KG/>`_
- `Logitech MK320 Keyboard/Mouse Combo <http://www.amazon.com/Logitech-Wireless-Desktop-MK320-Keyboard/dp/B003VAGXZC/ref=sr_1_1?s=electronics&ie=UTF8&qid=1371841107&sr=1-1&keywords=mk320>`_
- `Logitech MK710 Keyboard/Mouse Combo <http://www.amazon.com/Logitech-Wireless-Desktop-Keyboard-920-002416/dp/B0036E8V08/>`_
- `Logitech MK260 Keyboard/Mouse Combo <http://www.amazon.com/Logitech-Wireless-Combo-Keyboard-920-002950/dp/B004KSQANO>`_
- `Inland Keyboard and Mouse Combo <http://www.amazon.com/Inland-Wireless-2-4GHz-Optical-Keyboard/dp/B009V9IWCO/ref=sr_sp-btf_image_1_10?s=electronics&ie=UTF8&qid=1376403707&sr=1-10&keywords=inland+mouse+and+keyboard>`_
- `Solidtek KB-5010BU Keyboard+Roller Ball <http://www.logicsupply.com/products/kb_5010bu>`_

Mice
******

**Make sure that you plug the mice into the USB Host connector before powering on the board.**

- `Microsoft Wireless 1000 <http://www.amazon.com/Microsoft-Wireless-Mobile-Mouse-1000/dp/B003STDQQU/ref=sr_1_1?s=electronics&ie=UTF8&qid=1371841170&sr=1-1&keywords=microsoft+wireless+1000>`_
- `Logitech M705 <http://www.amazon.com/Logitech-Wireless-Marathon-Battery-910-001935/dp/B003TG75EG/>`_
- `Logitech M600 <http://www.amazon.com/Logitech-Touch-Mouse-M600-910-002666/dp/B006MBP7T0/>`_
- `Logitech M310 <http://www.logitech.com/en-us/product/wireless-mouse-m310>`_

USB HUBS
**********

**Make sure that you plug the HUB into the USB Host connector before powering on the board.**

- `Inland 4 Port <http://www.microcenter.com/product/360458/4-Port_USB_20_Hub>`_
- `Manhattan 10-port HUB <http://www.microcenter.com/product/393316/10-Port_USB_20_Hi-Speed_Desktop_Hub>`_
- `4-Port USB Cable HUB <http://www.microcenter.com/product/354122/4-Port_USB_20_Cable_Hub>`_
- `D-LINK DUB-H7 <http://www.dlink.com/us/en/home-solutions/connect/usb/dub-h7-7-port-usb-2-0-hub>`_
- `Trust HU-5770 7-Port Powered Hub <http://www.amazon.co.uk/TRUST-UK-HU-5770-PORT-POWERED/dp/B000HG5Q42>`_

.. tip::
    Make sure you are powering BeagleBone with decent power supply with 
    enough current before attaching any additional Peripherals. 
    See :ref:`accessories-power-supplies` for more information on power requirements.
