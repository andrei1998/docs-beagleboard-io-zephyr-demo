.. _beaglebone-ai-home:

BeagleBone AI
###############

.. admonition:: OSHWA Certification mark

    .. figure:: images/OSHW_mark_US000169.*
        :width: 200
        :target: https://certification.oshwa.org/us000169.html
        :alt: BeagleBone AI OSHW Mark

.. note::

    This work is licensed under a `Creative Commons Attribution-ShareAlike
    4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`__

    Hardware design files can be found at https://git.beagleboard.org/beagleboard/beaglebone-ai

.. tip:: 
    
    Make sure to read and accept all the terms & condition provided in the :ref:`boards-terms-and-conditions` page. 
    
    Use of either the boards or the design materials constitutes agreement to the T&C including any 
    modifications done to the hardware or software solutions provided by beagleboard.org foundation.

.. toctree::
    :maxdepth: 1

    ch01.rst
    ch02.rst
    ch03.rst
    ch04.rst
    ch05.rst
    ch06.rst
    ch07.rst
    ch08.rst
    ch09.rst
    ch10.rst
    ch11.rst
    ch12.rst

