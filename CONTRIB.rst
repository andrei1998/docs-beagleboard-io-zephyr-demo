.. _contributors:

Contributors
#############

This is a list of people who have made contributions to BeagleBoard.org unified documentation project.

People are listed alphabetically, as verified with Unix sort:

.. code-block:: shell

    $ grep "^- " CONTRIB.rst | LC_ALL=C sort -u -c -f


This is certainly not comprehensive, and if you've been overlooked (sorry!), please open an issue on GitLab or mention it on the forum.

- `Andrei Aldea <https://git.beagleboard.org/andrei1998>`_
- `Ayush Singh <https://git.beagleboard.org/ayush1325>`_
- Cathy Wicks
- `Deepak Khatri <https://git.beagleboard.org/lorforlinux>`_
- `Dhruva Gole <https://git.beagleboard.org/dhruvag2000>`_
- `Harshil Bhatt <https://git.beagleboard.org/harshilbhatt2001>`_
- `Jason Kridner <https://git.beagleboard.org/jkridner/>`_
- `Jian De <https://git.beagleboard.org/jiande>`_
- `Joshua Neal <https://git.beagleboard.org/jdneal>`_
- `Kai Yamada <https://git.beagleboard.org/gpioblink>`_
- `Krishna Narayanan <https://git.beagleboard.org/Krishna_13>`_
- `Mark A. Yoder <https://git.beagleboard.org/yoder>`_
- `Robert Nelson <https://git.beagleboard.org/RobertCNelson>`_
- `Sabeeh Khan <https://git.beagleboard.org/sabeeh.khan14>`_
- `Seth N <https://git.beagleboard.org/silver2row>`_